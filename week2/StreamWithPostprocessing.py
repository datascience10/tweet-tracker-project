import tweepy
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
import json
import config
import sys
from datetime import date
from textblob import TextBlob 



def strip_special(text):
	if text:
		try:
			return text.encode('ascii', 'ignore').decode('ascii').replace('\n', ' ')
		except:
			return text.encode('utf-8', 'ignore').replace('\n', ' ')
	else:
		return None

class ProcessingListener(StreamListener):

	def __init__(self, query):
		super(ProcessingListener, self).__init__()

		today = date.today()
		dt = today.strftime("%d-%m-%Y")
    
		fn = '_'.join(query)
		print(fn)
		self.outfile = dt + fn + '.csv'
		
		output = 'id_str\tcreated_at\ttext\tpolarity\tsubjectivity\tuser_created_at\tuser_location\tuser_description\tuser_followers_count\tretweet_count\tfavorite_count\n'
		try:
			with open(self.outfile, 'w+') as f:
				print('File created: {}'.format(self.outfile))

				f.write(output)
		except BaseException as e:
			print("Error on initial write: %s" % str(e))

	

	def on_status(self, status):

		if status.retweeted:
        	# Ignore retweets
			return True
    	# Extract attributes from each tweet
		id_str = status.id_str
		created_at = status.created_at
		text = strip_special(status.text)    # Pre-processing the text  
		sentiment = TextBlob(text).sentiment
		polarity = sentiment.polarity
		subjectivity = sentiment.subjectivity
        
		user_created_at = status.user.created_at
		user_location = strip_special(status.user.location)
		user_description = strip_special(status.user.description)
		user_followers_count =status.user.followers_count
        
		retweet_count = status.retweet_count
		favorite_count = status.favorite_count
     	# Quick check on preprocessed tweets
		print(text)

		try:
			with open(self.outfile, 'a') as f:			
				output = '{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'\
				.format(id_str, created_at, text, polarity, subjectivity,user_created_at,\
				 user_location, user_description, user_followers_count, retweet_count, favorite_count)
				print(output)
				f.write(output)

				#return True
		except BaseException as e:
			print("Error on_data: %s" % str(e))
			return True


if __name__ == '__main__':

	auth = OAuthHandler(config.consumer_key, config.consumer_secret)
	auth.set_access_token(config.access_token, config.access_token_secret)
	api = tweepy.API(auth)
	args = sys.argv
	print(args)
    
	if(len(args) <= 1):
		q = ['covid', 'covid19']
		twitter_stream = Stream(auth, ProcessingListener(q))
		twitter_stream.filter(track=q)
	else:
		totrack = args[1:]
		print(totrack)
		twitter_stream = Stream(auth, ProcessingListener(totrack))
		twitter_stream.filter(track=totrack)
	
	
