# Week 2
For week 2, the goal will be to format your stream and put it into an output file. 


## Part 1: Get familiar with the tweet object json that is streamed.
Details of the json returned from the stream can be found [here](https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object).

Using the tweepy library, you can access each of the fields. Try modifying your `on_status` function so that it doesn't just print out the whole stream object, but specific fields, e.g.
```
def on_status(self, status):
	print(status.text)
	print(status.user.created_at)
```

## Part 2: More advanced post-processing and handling encoding errors

Once you are more familiar with the object returned from streaming, you can introduce further postprocessing, e.g. some text processing:

```
text = strip_special(status.text) ## Remove space
sentiment = TextBlob(text).sentiment ## Use the textblob library to do some sentiment analysis
polarity = sentiment.polarity ## Get the tweet text's polarity
subjectivity = sentiment.subjectivity ## Get the tweet text's subjectivity
        
```
You might also encounter some encoding errors, which you will need to handle, e.g.:
```
try:
	return text.encode('ascii', 'ignore') ## Try ascii encoding and remove new lines.
except:
	return ''
```


Or a combination:
```
try:
	return text.encode('ascii', 'ignore').replace('\n', ' ') ## Try ascii encoding and remove new lines.
except:
	return ''
```

## Part 4: Decide on the format of your output file
You might want to write your formatted tweet data into a CSV or JSON file, or even a database, e.g.:
```
with open('afile.csv', 'a') as f:
	line_to_write = '{}\t{}\t{}\n'.format(text, polarity, subjectivity)
	f.write(line_to_write)
``` 
