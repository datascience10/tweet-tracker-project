import tweepy
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
import json
import config
import sys
from datetime import date



class FirstListener(StreamListener):

	def __init__(self, query):
		today = date.today()
		dt = today.strftime("%d-%m-%Y")
    
		fn = '_'.join(query)
		print(fn)
		self.outfile = dt + fn + '.json'
	
 
	def on_data(self, data):
		#print(data)
		try:
			with open(self.outfile, 'a') as f:
				f.write(data)
				return True
		except BaseException as e:
			print("Error on_data: %s" % str(e))
		return True
 
	def on_error(self, status):
		print(status)
		return True



if __name__ == '__main__':

	auth = OAuthHandler(config.consumer_key, config.consumer_secret)
	auth.set_access_token(config.access_token, config.access_token_secret)
	api = tweepy.API(auth)
	args = sys.argv
	print(args)
    
	if(len(args) <= 1):
		q = ['covid', 'covid19']
		twitter_stream = Stream(auth, FirstListener(q))
		twitter_stream.filter(track=q)
	else:
		totrack = args[1:]
		print(totrack)
		twitter_stream = Stream(auth, FirstListener(totrack))
		twitter_stream.filter(track=totrack)
	
	
