import tweepy
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
import json
import config
import sys



class FirstListener(StreamListener):
	
 
	def on_data(self, data):
		print(data)
		
 
	def on_error(self, status):
		print(status)
		return True



if __name__ == '__main__':

	#consumer_key = 'your_consumer_key'
	#consumer_secret = 'your_consumer_secret'
	#access_token = 'your_access_token'
	#access_token_secret = 'your_access_token_secret'

	consumer_key = config.consumer_key
	consumer_secret = config.consumer_secret
	access_token = config.access_token
	access_token_secret = config.access_token_secret

	auth = OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	

	twitter_stream = Stream(auth, FirstListener())
	twitter_stream.filter(track=['covid19', 'coronavirus'])
    
	
	
