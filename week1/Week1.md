# Week 1
For week 1 I thought we could start gently, so I've provided most of the code you'll need below. But it would be good for you to read some of the supporting API documentation and other references (links given below) so you understand what the code is doing and to get a feel for what is possible.

## Part 1: Getting set up
1. Make sure you have a twitter account. If you don't, sign up.
2. Register your app so you can use the API: https://developer.twitter.com/en/apps 
3. You will receive a consumer key and a consumer secret
4. (optional) You should really keep your consumer key, consumer secret, access token and access token secret. You can do this by putting them in a config.py file and not sharing this file or setting them as environment variables. See the [anaconda documentation on how to do this](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#setting-environment-variables)
5. Install tweepy 

## Part 2: Authenticating in your python script

To be able to use the API, you need to declare your authentication credentials and then use `OAuthHandler` to authenticate (this is a bit like signing in programatically):

```
import tweepy
from tweepy import OAuthHandler

##Declare your authentication credentials    
consumer_key = 'YOUR-CONSUMER-KEY'
consumer_secret = 'YOUR-CONSUMER-SECRET'
access_token = 'YOUR-ACCESS-TOKEN'
access_secret = 'YOUR-ACCESS-SECRET'
 
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

```
You can find more details on authentication and the various options available in the [API documentation](http://docs.tweepy.org/en/latest/auth_tutorial.html#auth-tutorial).


## Part 3: Start streaming tweets containing a particular keyword
To start getting data from twitter, you need to extend an API class called `StreamListener`. If you're not yet familiar with classes and objects in Python, [this tutorial](https://www.hackerearth.com/de/practice/python/object-oriented-programming/classes-and-objects-i/tutorial/) gives quite a good introduction.

Once you have created your own class which extends `StreamListener` (in the code snippet below, this is `FirstListener`), you need to define what happens in its `on_status` function. In the snippet below, all the data received will be printed out.

```
from tweepy import Stream
from tweepy.streaming import StreamListener
 
class FirstListener(StreamListener):
 
    def on_status(self, data):
        print(data)
 
 
twitter_stream = Stream(auth, FirstListener())
twitter_stream.filter(track=['covid'])
```

For more on streaming, you can again refer to the [API documentation](http://docs.tweepy.org/en/latest/streaming_how_to.html).
